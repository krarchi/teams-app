from flask import Blueprint,render_template
from application.utilities.util import required_params
from application.models.tasks import Task
from application.utilities.flask import APIError,APIResponse
from http import HTTPStatus


dash_bp = Blueprint("dashboard",__name__,template_folder="templates")
        


@dash_bp.route('/')
def home():
    return "<html>Welcome To Home Page</html>"

@dash_bp.route('/add_task',methods=['POST'])
@required_params(['name','created_by'])
def add_task(name,created_by):
    task = Task(name,created_by)
    task.save()
    return APIResponse(message="User Created",status=HTTPStatus.OK)

@dash_bp.route('/get', methods=['POST'])
@required_params(['id'])
def get(id):
    task = Task.get(id)
    response_data = {'name': task.name, 'created_by': Task.created_by , 'created_on': Task.created_on , 'status': 'completed' if Task.end_on else 'in progress'}
    return APIResponse(message="Task Returned", data=response_data, status=HTTPStatus.OK)

@dash_bp.route('/get_all', methods=['POST'])
@required_params(['created_by'])
def get_all(created_by):
    task_list = Task.get_by_creator(created_by)
    data = []
    for task in task_list:
        data.append({'name': task.name, 'created_by': Task.created_by , 'created_on': Task.created_on , 'status': 'completed' if Task.end_on else 'in progress'})
    return APIResponse(message="Task Returned", data={'data': data}, status=HTTPStatus.OK)