import base64
from Crypto.Cipher import AES
from application.configs.jwt import JWTConfig

def encrypt_password(clear_text):
    enc_secret = AES.new(JWTConfig.PASSWORD_ENCRYPTION_KEY[:32],AES.MODE_CBC,iv=JWTConfig.iv)
    tag_string = (
            str(clear_text)
            + (AES.block_size - len(str(clear_text)) % AES.block_size) * "\0"
    )
    cipher_text = base64.b64encode(enc_secret.encrypt(tag_string.encode()))
    return(cipher_text)

def decrypt_password(cipher_text):
    try:
        dec_secret = AES.new(JWTConfig.PASSWORD_ENCRYPTION_KEY[:32],AES.MODE_CBC,iv=JWTConfig.iv)
        raw_decrypted = dec_secret.decrypt(base64.b64decode(cipher_text))
        clear_val = raw_decrypted.decode().rstrip("\0")
        return clear_val
    except Exception as e:
        # capture_exception(e)
        return cipher_text
