from application import db
from datetime import datetime, timedelta


class Task(db.Model):
    _tablename__ = "tasks"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200))
    created_by = db.Column(db.String(200))
    created_on = db.Column(db.String, default = str(datetime.utcnow))
    end_on = db.Column(db.String)

    def __init__(self, name, created_by):
        self.name = name
        self.created_by = created_by
        self.created_on = str(datetime.utcnow)
    
    @classmethod
    def get(cls,id):
        return cls.query.filter(cls.id == id).first()
    
    @classmethod
    def get_by_creator(cls,created_by):
        return cls.query.filter(cls.created_by == created_by).all()
