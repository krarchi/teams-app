import os

DB_URI = "postgres://postgres:1234@localhost:5432/teams"

class DBConfig():
    SQLALCHEMY_DATABASE_URI = os.environ.get("DB_CONNECTION_STRING", os.environ.get("SQLALCHEMY_DATABASE_URI", DB_URI))
