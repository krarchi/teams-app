import os


class JWTConfig:
    USER_WEB_AUTH_API_SECRET = os.environ.get("USER_WEB_AUTH_API_SECRET", "USER_WEB_AUTH_API_SECRET")
    PASSWORD_ENCRYPTION_KEY = os.environ.get("PASSWORD_ENCRYPTION_KEY",b"e2gy547jur1q56ac")
    iv=os.environ.get("iv",b'0123456789abcdef')
